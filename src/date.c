/*
 * Authorship Statement
 *
 * Name: Ang Yi Liang
 * GUID: 2355327A
 * Assignment: APH Exercise 1
 *
 * This is my own work as defined in the Academic Ethics agreement I have signed.
 *
 */

#include "date.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>

#define MAX_DAY 31
#define MAX_MONTH 12
#define MAX_YEAR 9999
#define MIN_DAY 1
#define MIN_MONTH 1
#define MIN_YEAR 1

struct date{
	int collatedDate;
};

Date *date_create(char *datestr){

	Date *date;
	long i, dateCount = 0, day = 0, month = 0, year = 0;
	long bigMonth[7] = {1, 3, 5, 7, 8, 10, 12};
	long smallMonth[4] = {4, 6, 9, 11};

	/* check null, size, and format of date based on DD/MM/YYYY */
	if (datestr == NULL || strlen(datestr) != 10 || datestr[2] != '/' || datestr[5] !='/'){
		return NULL;
	}

	/* remove the slashes and check isdigit */
	for(i=0; i<10; i++){
		if(datestr[i] != '/' && isdigit(datestr[i])){
			dateCount++;
		}
	}
	if(dateCount != 8)
		return NULL;

	/* copy 1st and 2nd char for date */
	day = atoi(datestr);
	/* copy 4th and 5th char for month */
	month = atoi(datestr+3);
	/* copy last 4 char for year*/
	year = atoi(datestr+6);
	
	/* printf("Day: %d\n", day); 
	 * printf("Month: %d\n", month);
	 * printf("Year: %d\n", year);
	 */

	/* checking if day month and year are within the correct range. */
	if (day < MIN_DAY || day > MAX_DAY || month < MIN_MONTH || month > MAX_MONTH || year < MIN_YEAR || year > MAX_YEAR){
		return NULL;
	}

	/* checking if day and the month are correct
	 * check for big month */
	for (i=0; i < 7; i++){
		if(month ==  bigMonth[i] && day > 31)
			return NULL;
	}
	/* check for small month */
	for (i=0; i < 4; i++){
		if(month == smallMonth[i] && day > 30)
			return NULL;
	}

	/* check for February days */
	if (month == 2){
		/* leap year */
		if (( year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
			if (day > 29)
				return NULL;		
		}
		else
			if(day > 28)
				return NULL;
	}

	/* create a new date with the settings YYYYMMDD */
	date = malloc(sizeof(Date));
	if(date == NULL)
		return NULL;

	/* add in the year */
	date->collatedDate = (year*10000);

	/* add in the month */
	date->collatedDate += (month*100);

	/* add in the day */
	date->collatedDate += (day);
	/* printf("Date: %d\n", date->collatedDate); */
	return date;

}

Date *date_duplicate(Date *d){

	/* return if trying to duplicate a null */
	if(d == NULL)
		return NULL;
	
	Date *newDate = malloc(sizeof(Date));
	if(newDate == NULL)
		return NULL;
	newDate->collatedDate = d->collatedDate;
	return newDate;
}

int date_compare(Date *date1, Date *date2){

	/* return if eithr date is null */
	if(date1 == NULL || date2 == NULL)
		return NULL;

	return date1->collatedDate - date2->collatedDate;
}

void date_destroy(Date *d){
	free(d);
}
