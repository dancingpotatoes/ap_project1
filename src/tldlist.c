/*
 * Authorship Statement
 *
 * Name: Ang Yi Liang
 * GUID: 2355327A
 * Assignment: APH Exercise 1
 *
 * This is my own work as defined in the Academic Ethics agreement I have signed.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "tldlist.h"

struct tldlist{
	TLDNode *root;
	long hostCount;
	long hostnames;
	Date *begin;
	Date *end;
};

struct tldnode{
	char *hostname;
	long hostCount;
	long height;
	TLDNode *leftChild;
	TLDNode *rightChild;
	TLDNode *parent;
};

struct tlditerator{
	TLDNode **nodeList;
	long current;
	long total;
};

/* basic functions for BST */

/* 
 * create new node based on given hostname
 */
static TLDNode *tldnode_new(char *hostname);

/* add new node to the tree if hostname doesn't exist in the tree */
static void tldnode_add(TLDList *tld, char *hostname, TLDNode *node);

/* pre-order traversal */
static void tlditer_preorder(TLDIterator *iter, TLDNode *node);

/* get the max of two function */
static long max_between(long h1, long h2);


/* functions for AVL implementation */

/* update height values */
static void tldnode_update_height(TLDNode *node);

/* balance the tree */
static void tldnode_balance_tree(TLDList *tldL, TLDNode *node);

/* AVL left left case */
static void tldnode_rotate_LL(TLDList *tldL, TLDNode *node);

/* AVL right right case */
static void tldnode_rotate_RR(TLDList *tldL, TLDNode *node);

/* AVL left right case */
static void tldnode_rotate_LR(TLDList *tldL, TLDNode *node);

/* AVL right leftt case */
static void tldnode_rotate_RL(TLDList *tldL, TLDNode *node);

/* Check if node is not balance */
static int ifNotBalanced(TLDNode *node);

/* find the node that is not balanced */
static TLDNode *locateNotBalanced(TLDNode *node);



/*
 * tldlist_create generates a list structure for storing counts against
 * top level domains (TLDs)
 *
 * creates a TLDList that is constrained to the `begin' and `end' Date's
 * returns a pointer to the list if successful, NULL if not
 */
TLDList *tldlist_create(Date *begin, Date *end){

	if(begin == NULL || end == NULL) return NULL;

	TLDList* tldlist = malloc(sizeof(TLDList));
	if(tldlist == NULL)
		return NULL;

	if(tldlist != NULL){
		tldlist->root = NULL;
		tldlist->hostCount = 0;
		tldlist->hostnames = 0;
		tldlist->begin = begin;
		tldlist->end = end;
	}
	return tldlist;
}

/*
 * post order destroy will free all the nodes and its respective hostnames
 */
static void post_order_destroy(TLDNode *node){
	
	if(node == NULL)
		return;

	post_order_destroy(node->leftChild);
	post_order_destroy(node->rightChild);
	free(node->hostname);
	free(node);
}



/*
 * tldlist_destroy destroys the list structure in `tld'
 *
 * all heap allocated storage associated with the list is returned to the heap
 */
void tldlist_destroy(TLDList *tld){
	post_order_destroy(tld->root);
	free(tld);
}

int is_valid_tld(char *hostname){	

	char *tld  = strrchr(hostname, '.');

	if(tld == NULL)
		return 0;

	/* tld++ to get to the position after dot */
	tld++;
	int tldlen = strlen(tld);
	int i;

	/* check if tldlen is 0 */
	if(tldlen == 2 || tldlen == 3){

		/* check if all of tld char is alpha */
		for(i = 1; i < tldlen; i++){
			if(!isalpha(tld[0])){
				return 0;
			}
		}
		return 1;
	} else{
		return 0;
	}
}


/*
 * tldlist_add adds the TLD contained in `hostname' to the tldlist if
 * `d' falls in the begin and end dates associated with the list;
 * returns 1 if the entry was counted, 0 if not
 */
int tldlist_add(TLDList *tld, char *hostname, Date *d){

	if(tld == NULL || hostname == NULL || d == NULL)
		return 0;

	if (!is_valid_tld(hostname))
		return 0;

	/* get position of TLD in hostname */
	char *lwrTLD = strrchr(hostname, '.') + 1;
	if(lwrTLD == NULL)
		return 0;

	int i;

	for(i=0; lwrTLD[i]; i++)
		lwrTLD[i] = tolower(lwrTLD[i]);

	/* check if the date is within the limit */
	if(date_compare(d, tld->begin) >= 0 && date_compare(d, tld->end) <= 0){
		/* check if tree has a root */
		if(tld->root == NULL){
			tld->root = tldnode_new(lwrTLD);
			tld->hostnames++;
		} else{
			tldnode_add(tld, lwrTLD, tld->root);
		}
		tld->hostCount++;

		return 1;
	}	

	return 0;		

}

/*
 * tldlist_count returns the number of successful tldlist_add() calls since
 * the creation of the TLDList
 */
long tldlist_count(TLDList *tld){
	return tld->hostCount;
}

/*
 * preorder traversal of tree
 */
static void tlditer_preorder(TLDIterator *iter, TLDNode *node){
	if(node == NULL)
		return;

	(iter->nodeList)[iter->current] = node;
	(iter->current)++;
#if 0
	printf("preorder node: %s || current index: %d\n", node->hostname, *current);
#endif

	if((node->leftChild)!=NULL){
		tlditer_preorder(iter, node->leftChild);
	}

	if((node->rightChild)!=NULL){
		tlditer_preorder(iter, node->rightChild);
	}
}


/*
 * tldlist_iter_create creates an iterator over the TLDList; returns a pointer
 * to the iterator if successful, NULL if not
 */
TLDIterator *tldlist_iter_create(TLDList *tld){

	TLDIterator *newIter = malloc(sizeof(TLDIterator));
	if (newIter == NULL) 
		return NULL;

	TLDNode **nodeList = malloc(sizeof(TLDNode*)*(tld->hostnames));
	if(nodeList == NULL){
		free(newIter);
		return NULL;
	}

	newIter->nodeList = nodeList;
	newIter->current = 0;
	newIter->total = tld->hostnames;


	tlditer_preorder(newIter, tld->root);
	newIter->current = 0;
	return newIter;
}

/*
 * tldlist_iter_next returns the next element in the list; returns a pointer
 * to the TLDNode if successful, NULL if no more elements to return
 */
TLDNode *tldlist_iter_next(TLDIterator *iter){

	if(iter == NULL)
		return NULL;
	/* check if the current node on list is the last */
	if(iter->current >= iter->total){
		iter->current = 0;
		return NULL;	
	}

	TLDNode * node = (iter->nodeList)[iter->current++];

#if 0
	printf("current iter: %d || total iter: %d\n", iter->current, iter->total);
	printf("current iter next node: %s || count = %d\n", node->hostname, node->hostCount);
#endif

	/* return the next node on the list */
	return node;
}

/*
 * tldlist_iter_destroy destroys the iterator specified by `iter'
 */
void tldlist_iter_destroy(TLDIterator *iter){
	free(iter->nodeList);
	free(iter);
}

/*
 * tldnode_tldname returns the tld associated with the TLDNode
 */
char *tldnode_tldname(TLDNode *node){
	return node->hostname;
}

/*
 * tldnode_count returns the number of times that a log entry for the
 * corresponding tld was added to the list
 */
long tldnode_count(TLDNode *node){
	return node->hostCount;
}


/* 
 * create new node based on given hostname
 */
static TLDNode *tldnode_new(char *hostname){
	TLDNode *newNode = malloc(sizeof(TLDNode));
	if(newNode == NULL){
		return NULL;
	}

	newNode->hostname = malloc(strlen(hostname) + 1);
	if(newNode->hostname == NULL){
		free(newNode);
		return NULL;
	}

	strcpy(newNode->hostname, hostname);
	newNode->hostCount = 1;
	newNode->height = 1;
	newNode->leftChild = NULL;
	newNode->rightChild = NULL;
	newNode->parent = NULL;
	return newNode;
}

/*
 * add new node to the treee
 * 
 */
static void tldnode_add(TLDList *tld, char *hostname, TLDNode *node){

	int compareHost;

	compareHost = strcmp(hostname, node->hostname);
	if(compareHost < 0){
		/* if new hostname lesser than current hostname, go left child */
		if(node->leftChild != NULL) {
			tldnode_add(tld, hostname, node->leftChild);
		} else {
			node->leftChild = tldnode_new(hostname);
			node->leftChild-> parent = node;
#if 0
			printf("tldlist added: %s to left of %s\n", node->leftChild->hostname, node->hostname);
#endif
			tldnode_update_height(node->leftChild);
			tldnode_balance_tree(tld, node->leftChild);
			tld->hostnames++;

		} 
	} else if(compareHost > 0){
		/* if new hostname more than current hostname, go right child */
		if(node->rightChild != NULL) {
			tldnode_add(tld, hostname, node->rightChild);
		} else {
			node->rightChild = tldnode_new(hostname);
			node->rightChild->parent = node;
#if 0
			printf("tldlist added: %s to right of %s\n", node->rightChild->hostname, node->hostname);
#endif
			tldnode_update_height(node->rightChild);
			tldnode_balance_tree(tld, node->rightChild);
			tld->hostnames++;
		}
	} else {
		node->hostCount++;
#if 0
		printf("tldlist added: %s incremented \n", node->hostname);
#endif
	}

}

/* get the max of two function */
static long max_between(long h1, long h2){
	if(h1 >= h2)
		return h1;
	else
		return h2;
}

/* functions for AVL implementation */

/* update height values */
static void tldnode_update_height(TLDNode *node){
	if(node != NULL){
		if(node->leftChild != NULL && node->rightChild != NULL)
			node->height = max_between(node->leftChild->height, node->rightChild->height) + 1;
		else if(node->leftChild != NULL)
			node->height = (node->leftChild->height) + 1;
		else if(node->rightChild != NULL)
			node->height = (node->rightChild->height) + 1;
		else
			node->height = 1;
		tldnode_update_height(node->parent);
	}
}

/* get height values */
static long get_left_height(TLDNode *node){
	if(node == NULL || node->leftChild == NULL)
		return 0;
	else 
		return node->leftChild->height;
}

static long get_right_height(TLDNode *node){
	if(node == NULL || node->rightChild == NULL)
		return 0;
	else 
		return node->rightChild->height;
}

static long get_left_left_height(TLDNode *node){
	if(node == NULL || node->leftChild == NULL || node->leftChild->leftChild == NULL)
		return 0;
	else 
		return node->leftChild->leftChild->height;
}

static long get_left_right_height(TLDNode *node){
	if(node == NULL || node->leftChild == NULL || node->leftChild->rightChild == NULL)
		return 0;
	else 
		return node->leftChild->rightChild->height;
}

static long get_right_left_height(TLDNode *node){
	if(node == NULL || node->rightChild == NULL || node->rightChild->leftChild == NULL)
		return 0;
	else 
		return node->rightChild->leftChild->height;
}

static long get_right_right_height(TLDNode *node){
	if(node == NULL || node->rightChild == NULL || node->rightChild->rightChild == NULL)
		return 0;
	else 
		return node->rightChild->rightChild->height;
}

/* balance the tree */
static void tldnode_balance_tree(TLDList *tldL, TLDNode *node){	

	TLDNode *problem = locateNotBalanced(node->parent);

	if(problem == NULL){ 
		return;
	} else{
#if 0
		printf("balancing problem: %s\n", problem->hostname);
#endif
		if(get_left_height(problem) > get_right_height(problem)){
			if(get_left_left_height(problem) > get_left_right_height(problem)){
				tldnode_rotate_LL(tldL, problem);
			} else if(get_left_left_height(problem) < get_left_right_height(problem)) {
				tldnode_rotate_LR(tldL, problem);
			} 
		} else if(get_left_height(problem) < get_right_height(problem)){
			if(get_right_left_height(problem) > get_right_right_height(problem)){
				tldnode_rotate_RL(tldL, problem);
			} else if(get_right_left_height(problem) < get_right_right_height(problem)){
				tldnode_rotate_RR(tldL, problem);
			}
		}
	}
}

/* AVL left left case */
static void tldnode_rotate_LL(TLDList *tldL, TLDNode *node){

#if 0
	printf("performing LL\n");
#endif
	TLDNode *problemNode, *leftNode, *parentNode;
	problemNode = node;
	leftNode = node->leftChild;
	parentNode = node->parent;

	if(parentNode == NULL){
		tldL->root = leftNode;
		leftNode->parent == NULL;
	} else if(problemNode == (problemNode->parent->leftChild)){
		parentNode->leftChild = leftNode;
	} else {
		parentNode->rightChild = leftNode;
	}

	problemNode->leftChild = leftNode->rightChild;
	if(leftNode->rightChild != NULL)
		leftNode->rightChild->parent = problemNode;

	problemNode->parent = leftNode;
	leftNode->parent = parentNode;
	leftNode->rightChild = problemNode;

	tldnode_update_height(problemNode);	
}


/* AVL right right case */
static void tldnode_rotate_RR(TLDList *tldL, TLDNode *node){

#if 0
	printf("performing RR\n");
#endif
	TLDNode *problemNode, *rightNode, *parentNode;
	problemNode = node;
	rightNode = node->rightChild;
	parentNode = node->parent;

	if(parentNode == NULL){
		tldL->root = rightNode;
		rightNode->parent == NULL;
	} else if(problemNode == (problemNode->parent->leftChild)){
		parentNode->leftChild = rightNode;
	} else {
		parentNode->rightChild = rightNode;
	}


	problemNode->rightChild = rightNode->leftChild;

	if(rightNode->leftChild != NULL){
		rightNode->leftChild->parent = problemNode;
	}

	problemNode->parent = rightNode;
	rightNode->parent = parentNode;
	rightNode->leftChild = problemNode;

	tldnode_update_height(problemNode);	
}



/* AVL left right case */
static void tldnode_rotate_LR(TLDList *tldL, TLDNode *node){

#if 0
	printf("performing LR\n");
#endif
	TLDNode *problemNode, *leftNode, *rightNode, *parentNode;
	problemNode = node;	
	leftNode = problemNode->leftChild;
	rightNode = leftNode->rightChild;
	parentNode = problemNode->parent;

	if(parentNode == NULL){
		tldL->root = rightNode;
		rightNode->parent = NULL;
	} else if(problemNode == (parentNode->leftChild)){
		parentNode->leftChild = rightNode;
		rightNode->parent = parentNode;
	} else {
		/* if problem node is the right child of its parent */
		parentNode->rightChild = rightNode;
		rightNode->parent = parentNode;
	} 

	problemNode->leftChild = rightNode->rightChild;
	if(rightNode->rightChild != NULL)
		rightNode->rightChild->parent = problemNode;

	leftNode->rightChild = rightNode->leftChild;
	if(rightNode->leftChild != NULL)
		rightNode->leftChild->parent = leftNode;

	rightNode->leftChild = leftNode;
	rightNode->rightChild = problemNode;
	leftNode->parent = rightNode;
	problemNode->parent = rightNode;

	tldnode_update_height(leftNode);	
	tldnode_update_height(problemNode);	

}

/* AVL right left case */
static void tldnode_rotate_RL(TLDList *tldL, TLDNode *node){

#if 0
	printf("performing RL\n");
#endif
	TLDNode *problemNode, *leftNode, *rightNode, *parentNode;
	problemNode = node;	
	rightNode = problemNode->rightChild;
	leftNode = rightNode->leftChild;
	parentNode = problemNode->parent;

	if(parentNode == NULL){
		tldL->root = leftNode;
		leftNode->parent = NULL;
	} else if(problemNode == (parentNode->leftChild)){
		parentNode->leftChild = leftNode;
		leftNode->parent = parentNode;
	} else {
		/* if problem node is the right child of its parent */
		parentNode->rightChild = leftNode;
		leftNode->parent = parentNode;
	} 

	rightNode->leftChild = leftNode->rightChild;
	if(leftNode->rightChild != NULL)
		leftNode->rightChild->parent = rightNode;

	problemNode->rightChild = leftNode->leftChild;
	if(leftNode->leftChild != NULL)
		leftNode->leftChild->parent = problemNode;

	leftNode->leftChild = problemNode;
	leftNode->rightChild = rightNode;
	rightNode->parent = leftNode;
	problemNode->parent = leftNode;

	tldnode_update_height(rightNode);	
	tldnode_update_height(problemNode);	

}



/* Check if node is not balance */
static int ifNotBalanced(TLDNode *node){
	if(node->leftChild != NULL && node->rightChild != NULL){

		long diff = get_left_height(node) - get_right_height(node);
#if 0
		printf("diff of node '%s' : %d\n", node->hostname, diff);
#endif

		if(diff< -1 || diff > 1)
			return 1;
		else
			return 0;
	} else{

		if(node->leftChild != NULL && get_left_height(node) > 1)
			return 1;
		else if(node->rightChild != NULL && get_right_height(node) > 1)
			return 1;
		else
			return 0;
	}
}

/* find the node that is not balanced */
static TLDNode *locateNotBalanced(TLDNode *node){
	if(node == NULL)
		return NULL;
	else if(ifNotBalanced(node)){
#if 0
		printf("imbalance node: %s\n", node->hostname);
#endif
		return node;
	}
	else 
		return locateNotBalanced(node->parent);
}

